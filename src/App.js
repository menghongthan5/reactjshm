
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ButtonGroup from './component/ButtonGroup'
import { Col, Container, Row } from 'react-bootstrap';
import NavMenu from './component/NavMenu';
import Item from './component/Item'

function App() {
  return (
    <div className="App">
    <Container> 
      
      <NavMenu />
      <ButtonGroup />
   
    <Row>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
      <Col md={3}>
      <Item />
      </Col>
    </Row>
   
    </Container>
    
    </div>
  );
}

export default App;
