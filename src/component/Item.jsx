import React from 'react'
import {Card,Button} from 'react-bootstrap'
export default function Item() {
    return (
        <div>
            <Card style={{ width: '15rem' }}>
  <Card.Img variant="top" src="img/download.jpg" />
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Button variant="primary">Delets</Button>
  </Card.Body>
</Card>
        </div>
    )
}
